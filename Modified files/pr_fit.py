#!/usr/bin/env python3

import ROOT
import numpy as np
from array import array
import random
import os
import pandas as pd
import time

from fire import Fire
import importlib # dynamic import

import sys # to unload

import prun   # this seems like mutual import, but it is not really

# ------------------------- MINUIT PART ----------------
#  pip3 install iminout  numba_stats numpy
from iminuit import cost, Minuit
import iminuit
from numba_stats import norm, uniform # faster replacements for scipy.stats functions



def main( *args ):
    if len(args)<1:
        print("X... no object name given  (and no model given too)")
        return
    if len(args)<2:
        print("X... no model given")
        return

    fname = args[0]
    g_orig = ROOT.gDirectory.FindObject(f"{fname}")

    # - check the object. If None => try to load something
    #        AUTOMATIC LOAD
    #
    if g_orig==None:
        print(f"X... {fname} object doesnot exist in gDirectory")
        # - do we try to load it? If it is an actual file
        if os.path.exists(fname):
            print(f"i... BUT file /{fname} exists")
            print(f" ...       trying to unload and load /pr_load/")
            try:
                sys.modules.pop( "pr_load" )
            except:
                pass
            module_load = importlib.import_module("pr_load")
            ok = False
            try:
                if len(args)>1: # accept y,x as 2nd parameter
                    module_load.main(fname, args[3])
                else:
                    module_load.main(fname)
                ok = True
            except:
                ok = False
            if not ok:
                return
            fname = os.path.splitext(fname)[0]
            print(fname)
            g_orig = ROOT.gDirectory.FindObject(fname)
            print(g_orig)

    model_1 = f"pr_model_{args[1]}"
    if not(os.path.exists(f"{model_1}.py")):
        print(f"X... required model file  {model_1}.py does not exist")
        return

    # UNimport module first - case there was an error there previously
    try:
        # -------- I must unload to be able to edit the source whil being in CLING
        print(f" ...       trying to unload model  {model_1}  at first")
        sys.modules.pop(model_1)
    except:
        print(f" ...       model  {model_1} was not imported previously")
    module_1 = importlib.import_module(model_1)

    model_2 = f"pr_model_{args[2]}"
    if not(os.path.exists(f"{model_2}.py")):
        print(f"X... required model file  {model_2}.py does not exist")
        return

    # UNimport module first - case there was an error there previously
    try:
        # -------- I must unload to be able to edit the source whil being in CLING
        print(f" ...       trying to unload model  {model_2}  at first")
        sys.modules.pop(model_2)
    except:
        print(f" ...       model  {model_2} was not imported previously")
    module_2 = importlib.import_module(model_2)

    print(f"i... extracting /{g_orig.GetName()}/ of type /{g_orig.ClassName()}/")

    # -------------------------------   get Arrays -> import to numpy  .asarray
    if g_orig.ClassName()=="TGraph":
        x=np.asarray(g_orig.GetX())
        y=np.asarray(g_orig.GetY())
        dx = np.zeros_like(x)
        dy = np.zeros_like(y)+1
        print("!... UNIT ERROR SET ON Y-AXIS !!")

    if g_orig.ClassName() == "TGraphErrors":
        x = np.asarray(g_orig.GetX())
        y = np.asarray(g_orig.GetY())
        dx = np.zeros_like(x)
        dy = np.asarray(g_orig.GetEY())
        for i in range(0, len(dy)):
            if dy[i] < 0.002:
                dy[i] = 0.002

    # sets indexes of data, which energy is approx. in range (150 - 350)
    index_low = np.argwhere(x == min(x, key=lambda a: abs(a - 80)))[0][0]
    if index_low < 3:
        index_low = 3
    index_high = np.argwhere(x == min(x, key=lambda a: abs(a - 300)))[0][0]

    # definition of dataframe, where will be all fits
    df = pd.DataFrame(columns=['split_index', 'split_energy', 'chi_red_1', 'parameters_1', 'chi_red_2', 'parameters_2'])

    #-*********************************now the MINUIT PART
    #-*********************************now the MINUIT PART
    for energy_index in range(index_low, index_high):
        # FIRST HALF
        yf_1 = module_1.main(np.log(x[:energy_index+1]),np.log(y[:energy_index+1]),np.log(dy[:energy_index+1]))
        # SECOND HALF
        yf_2 = module_2.main(np.log(x[energy_index-1:]),np.log(y[energy_index-1:]),np.log(dy[energy_index-1:]))
        stored = [energy_index, x[energy_index], yf_1[0], yf_1[1], yf_2[0], yf_2[1]]
        df.loc[len(df)] = stored
    # dataframe will be sorted according to the reduced chi^2
    df = df.sort_values(by=['chi_red_1'])
    df = df.reset_index()
    print(df)
    first_formula = ROOT.TFormula("f1", 'exp([0]*log(x)*log(x)+[1]*log(x)+[2])')
    second_formula = ROOT.TFormula("f2", 'exp([3]*log(x)+[4])')
    diff_formula = ROOT.TFormula("fdif", 'abs(f1-f2)')

    first_func = ROOT.TF1('pol2', 'f1', 0, 3000)
    second_func = ROOT.TF1('pol1', 'f2', 0, 3000)
    difference = ROOT.TF1('diff', "fdif", 80, 500)

    # searches for the row with chi2_red == min for the first function
    # split_row = df['chi_red_1'].idxmin()

    # defines the point of intersection of two curves
    intersection = 0
    limit_c = 5*10 ** (-6)
    limit_d = 1.5*10 ** (-4)

    # search for the best combination of fits and the best intersection
    for i in range(0, len(df['split_index'])):

        # upload new parameters
        first_parameters = np.asarray(df['parameters_1'][i])
        first_func.SetParameters(first_parameters[0], first_parameters[1], first_parameters[2])
        second_parameters = np.asarray(df['parameters_2'][i])
        second_func.SetParameters(second_parameters[0], second_parameters[1])
        difference.SetParameters(first_parameters[0], first_parameters[1], first_parameters[2],
                                 second_parameters[0], second_parameters[1])

        # test whether there is intersection and search for the x_coordinate
        # if there is, it finds only first intersection, but probably there will be two
        x_coord_1 = difference.GetMinimumX(df['split_energy'][i]-20, difference.GetXmax())
        print(x_coord_1)
        y_coord_1 = difference.Eval(x_coord_1)
        print(y_coord_1)
        # derivatives at x_coord_1 for both functions
        deriv_1_first = first_func.Derivative(x_coord_1, params=first_parameters)
        deriv_1_second = second_func.Derivative(x_coord_1, params=second_parameters)
        deriv_1_diff = np.abs(deriv_1_first-deriv_1_second)
        print(deriv_1_diff)
        # this should be the second one
        x_coord_2 = difference.GetMinimumX(x_coord_1+10, difference.GetXmax())
        print(x_coord_2)
        y_coord_2 = difference.Eval(x_coord_2)
        print(y_coord_2)

        # derivatives at x_coord_2 for both functions
        deriv_2_first = first_func.Derivative(x_coord_2, params=first_parameters)
        deriv_2_second = second_func.Derivative(x_coord_2, params=second_parameters)
        deriv_2_diff = np.abs(deriv_2_first-deriv_2_second)
        print(deriv_2_diff)
        print("\n")

        if (y_coord_1 < limit_c) and (y_coord_2 < limit_c):
            if (deriv_1_diff < limit_d) and (deriv_2_diff < limit_d):
                print("\nDerivatives at both intersections are equal to zero.\n"
                      "The second point was chosen automatically.\n"
                      "For change, please, improve the code.\n")
                intersection = x_coord_2
                break
            elif (deriv_1_diff < limit_d) and (deriv_2_diff > limit_d):
                intersection = x_coord_1
                break
            elif (deriv_1_diff > limit_d) and (deriv_2_diff < limit_d):
                intersection = x_coord_2
                break
            else:
                continue
        elif (y_coord_1 < limit_c) and (y_coord_2 > limit_c):
            if deriv_1_diff < limit_d:
                intersection = x_coord_1
                break
            else:
                continue
        elif (y_coord_1 > limit_c) and (y_coord_2 < limit_c):
            if deriv_2_diff < limit_d:
                intersection = x_coord_2
                break
            else:
                continue
        else:
            continue

    print(intersection)
    y_fit = []
    # evaluation of y_fit values
    for i in range(0, len(x)):
        if x[i] <= intersection:
            y_fit.append(first_func.Eval(x[i]))
        else:
            y_fit.append(second_func.Eval(x[i]))
    y_fit = np.asarray(y_fit, dtype='d').flatten("C")

    # THIS PART CAN BE DELETED, IT IS FOR NOTHING
    # derivatives_1 = []
    # derivatives_2 = []
    # diff = []
    # first_part = []
    # for energy in range(10, 3000):
    #     first_part.append(energy)
    #     aaa = first_func.Derivative(energy, params=first_parameters)
    #     bbb = second_func.Derivative(energy, params=second_parameters)
    #     derivatives_1.append(aaa)
    #     derivatives_2.append(bbb)
    #     diff.append(np.abs(aaa-bbb))
    #
    # first_part = np.asarray(first_part, dtype='d').flatten("C")
    # derivatives_1 = np.asarray(derivatives_1, dtype='d').flatten("C")
    # derivatives_2 = np.asarray(derivatives_2, dtype='d').flatten("C")
    # diff = np.asarray(diff, dtype='d').flatten("C")

    #-*********************************now the MINUIT PART
    #-*********************************now the MINUIT PART
    # ///////////////////////////////////////////////////// plotting results
    cmain = ROOT.gPad.GetCanvas()  # reset all canvas
    cmain.Clear()
    cmain.Divide(1,2) # div canvas
    cmain.cd(1)

    g_orig.SetMarkerStyle(7) # small circle , no lines...
    g_orig.Draw("PAW")
    first_func.SetRange(0, intersection+2)
    first_func.SetLineColor(2)
    first_func.DrawCopy("sameL")
    second_func.SetRange(intersection-2, 3000)
    second_func.SetLineColor(4)
    second_func.DrawCopy("sameL")

    ROOT.gPad.SetGrid()
    ROOT.gPad.SetLogy()
    ROOT.gPad.SetLogx()
    #

    cmain.cd(2) # --------second pad
    # ------ differences ------------------

    gf = ROOT.TGraph(len(x), x.flatten("C"), y_fit.flatten("C"))
    gfy = np.asarray(gf.GetY())
    diffy = y-gfy
    gf_diff= ROOT.TGraphErrors(len(x), x.flatten("C"), diffy.flatten("C"), dx.flatten("C"), dy.flatten("C"))

    # keep the axes' labels from original graph
    newtitle =  f"exp-fit:{g_orig.GetTitle()};{g_orig.GetXaxis().GetTitle()};{g_orig.GetYaxis().GetTitle()}-fit"
    print(f"NEWTITLE={newtitle}")
    gf_diff.SetTitle( newtitle)
    gf_diff.SetMarkerStyle(7) # small circle , no lines...
    gf_diff.SetLineColor(4)
    gf_diff.Draw("PAW")
    ROOT.gPad.SetLogx()

    zeroy = y-y
    gf_zero= ROOT.TGraph(len(x), x.flatten("C"), zeroy.flatten("C"))
    gf_zero.SetLineColor(2)
    gf_zero.Draw("same")
    ROOT.gPad.SetGrid()

    #-------------------------- I NEED to REGISTER all to be able to display on gPad
    prun.register(first_func, "first")
    prun.register(second_func, "second")
    prun.register(gf, "fit")
    prun.register(gf_diff, "fitdiff")
    prun.register(gf_zero, "linezero")

    name = "{}".format(args[4])
    file = open(name, 'a')
    line = "{}\t{}\t{}\t{}\t{}\t{}\n".format(intersection,first_parameters[0], first_parameters[1], first_parameters[2],
                                 second_parameters[0], second_parameters[1])
    file.write(line)
    file.close()

if __name__=="__main__":
    Fire(main)
    # update canvas
    ROOT.gSystem.ProcessEvents()
    ROOT.gPad.Modified()
    ROOT.gPad.Update()
    ROOT.gPad
    input('press ENTER to end...')
